package edu.itpu.household_appliances_warehouse;

import edu.itpu.household_appliances_warehouse.controller.Controller;
import edu.itpu.household_appliances_warehouse.entity.Appliance;
import edu.itpu.household_appliances_warehouse.dao.DishWasherDao;
import edu.itpu.household_appliances_warehouse.entity.DishWasher;
import edu.itpu.household_appliances_warehouse.services.ApplianceServicesSearch;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
             Controller.main();
        } catch (IOException err) {
            throw new RuntimeException(err);
        }
    }
}
