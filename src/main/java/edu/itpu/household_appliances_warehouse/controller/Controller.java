package edu.itpu.household_appliances_warehouse.controller;

import edu.itpu.household_appliances_warehouse.dao.*;
import edu.itpu.household_appliances_warehouse.entity.Appliance;
import edu.itpu.household_appliances_warehouse.entity.DishWasher;
import edu.itpu.household_appliances_warehouse.layouts.SearchLayout;
import edu.itpu.household_appliances_warehouse.services.ApplianceServicesSearch;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Controller {

    public static void main() throws IOException {
        ApplianceServicesSearch applianceServicesSearch;
        DishWasherDao dishWasherDao = new DishWasherDao();
        MicroWaveDao microWaveDao = new MicroWaveDao();
        RefrigeratorDao refrigeratorDao = new RefrigeratorDao();
        TelevisionDao televisionDao = new TelevisionDao();
        WashingMachineDao washingMachineDao = new WashingMachineDao();

        System.out.println("""
                Project:    HOUSEHOLD APPLIANCES WAREHOUSE;
                Dev:        Mirakhmedov Mirsaid, mirsaid_mirakhmedov@student.itpu.uz;
                CreationStartedAt: May 17, 2023;
                CreationFinishedAt: Jun 09, 2023;
                """);
        projectEngine:while (true) {
            System.out.println("""
                        :Menu (available commands)
                        1.See all products
                        2.Search for product
                        3.Exit
                        """);
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();

            switch (command.toLowerCase()) {
                case "1": {
                    System.out.println("""
                            : Which product category you want to see :
                            1.Dishwasher
                            2.Microwave
                            3.Refrigerator
                            4.Television
                            5.Washing machine
                            6.Exit to main menu
                            """);
                    Scanner searchScanner = new Scanner(System.in);
                    String searchCommand = searchScanner.nextLine();
                    searchScanner = new Scanner(System.in);
                    if(searchCommand.equals("1") || searchCommand.equalsIgnoreCase("Dishwasher")) {
                        System.out.println(dishWasherDao.getAppliancesList());
                    } else if(searchCommand.equals("2") || searchCommand.equalsIgnoreCase("Microwave")) {
                        System.out.println(microWaveDao.getAppliancesList());

                    } else if(searchCommand.equals("3") || searchCommand.equalsIgnoreCase("Refrigerator")) {
                        System.out.println(refrigeratorDao.getAppliancesList());

                    } else if(searchCommand.equals("4") || searchCommand.equalsIgnoreCase("Television")) {
                        System.out.println(televisionDao.getAppliancesList());

                    } else if(searchCommand.equals("5") || searchCommand.equalsIgnoreCase("Washing machine")) {
                        System.out.println(washingMachineDao.getAppliancesList());

                    } else if(searchCommand.equals("5") || searchCommand.equalsIgnoreCase("Washing machine")) {
                        System.out.println("exit to main menu");
                    } else {
                        System.out.println("Invalid command, please checkout menu commands");
                    }
                } break;
                case "2": {
                    System.out.println("""
                            : Which product category you want to search :
                            1.Dishwasher
                            2.Microwave
                            3.Refrigerator
                            4.Television
                            5.Washing machine
                            6.Exit to main menu
                            """);
                    Scanner searchScanner = new Scanner(System.in);
                    String searchCommand = searchScanner.nextLine();
                    searchScanner = new Scanner(System.in);
                    SearchLayout searchLayout = new SearchLayout();
                    if(searchCommand.equals("1") || searchCommand.equalsIgnoreCase("Dishwasher")) {
                        searchLayout.innerSearch(dishWasherDao.getAppliancesList());
                    } else if(searchCommand.equals("2") || searchCommand.equalsIgnoreCase("Microwave")) {
                        searchLayout.innerSearch(microWaveDao.getAppliancesList());
                    } else if(searchCommand.equals("3") || searchCommand.equalsIgnoreCase("Refrigerator")) {
                        searchLayout.innerSearch(refrigeratorDao.getAppliancesList());
                    } else if(searchCommand.equals("4") || searchCommand.equalsIgnoreCase("Television")) {
                        searchLayout.innerSearch(televisionDao.getAppliancesList());
                    } else if(searchCommand.equals("5") || searchCommand.equalsIgnoreCase("Washing machine")) {
                        searchLayout.innerSearch(washingMachineDao.getAppliancesList());
                    } else {
                        System.out.println("Invalid command, please checkout menu commands");
                    }
                } break;
                case "3": {
                    break projectEngine;
                }
                default: {
                    System.out.println("Invalid command, please checkout menu commands");
                }
            }
        }
//        Appliance app = new Appliance("dishwasher",384,"dishwasher bosch", "bosch",3224521, 60.21,90,45,22);
//        DishWasherDao dish = new DishWasherDao();
//        System.out.println(dishwash.getSearchByBrand("artel").get(0));
    }
}
