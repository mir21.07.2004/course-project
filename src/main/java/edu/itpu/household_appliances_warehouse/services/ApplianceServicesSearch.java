package edu.itpu.household_appliances_warehouse.services;

import edu.itpu.household_appliances_warehouse.entity.Appliance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class ApplianceServicesSearch<T extends Appliance> implements ApplianceServices{
    private final List<T> listOfProducts;

    public ApplianceServicesSearch(List<T> listOfProducts1){
        this.listOfProducts = listOfProducts1;
    }

    @Override
    public List<T> getAllAppliances() throws IOException {
        return listOfProducts;
    }

    @Override
    public List<T> getSearchByBrand(String brand) throws IOException {
        Predicate<T> predicate = obj -> obj.getBrand().equalsIgnoreCase(brand);
        List<T> filteredResult = listOfProducts.stream().filter(predicate).toList();
        if(filteredResult.size() > 0) {
            return filteredResult;
        }
        System.out.println("Sorry, we couldn't find such brand... " + brand);
        return new ArrayList<>();
    }

    @Override
    public List<T> getSearchByCondition(String condition) throws IOException {
        Predicate<T> predicate = obj -> obj.getProductType().equalsIgnoreCase(condition);
        List<T> filteredResult = listOfProducts.stream().filter(predicate).toList();
        if(filteredResult.size() > 0) {
            return filteredResult;
        }
        System.out.println("Sorry, we have only two types New and Used");
        return new ArrayList<>();
    }

    @Override
    public List<T> getSearchById(int id) throws IOException {
        Predicate<T> predicate = obj -> obj.getProductId() == id;
        List<T> filteredResult = listOfProducts.stream().filter(predicate).toList();
        if(filteredResult.size() > 0) {
            return filteredResult;
        }
        System.out.println("Sorry, productId=" + id + " don't exist in our store");
        return new ArrayList<>();
    }

    @Override
    public List<T> getSearchByName(String name) throws IOException {
        Predicate<T> predicate = obj -> obj.getName().equalsIgnoreCase(name);
        List<T> filteredResult = listOfProducts.stream().filter(predicate).toList();
        if(filteredResult.size() > 0) {
            return filteredResult;
        }
        System.out.println("Sorry, product don't exist with the name " + name);
        return new ArrayList<>();
    }

    @Override
    public List<T> getSearchByPrice(long min, long max) throws IOException {
        Predicate<T> predicate = obj -> min <= obj.getPrice() && max >= obj.getPrice();
        List<T> filteredResult = listOfProducts.stream().filter(predicate).toList();
        if(filteredResult.size() > 0) {
            return filteredResult;
        }
        System.out.println("Please, enter valid price range");
        return new ArrayList<>();
    }
}
