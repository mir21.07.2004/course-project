package edu.itpu.household_appliances_warehouse.services;

import edu.itpu.household_appliances_warehouse.entity.Appliance;

import java.io.IOException;
import java.util.List;

public interface ApplianceServices <T>{
    List<T> getAllAppliances() throws IOException;
    List<T> getSearchByName(String name) throws IOException;
    List<T> getSearchById(int id) throws IOException;
    List<T> getSearchByBrand(String brand) throws IOException;
    List<T> getSearchByPrice(long min, long max) throws IOException;
    List<T> getSearchByCondition(String condition) throws IOException;
}