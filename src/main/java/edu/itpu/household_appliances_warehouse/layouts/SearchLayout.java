package edu.itpu.household_appliances_warehouse.layouts;

import edu.itpu.household_appliances_warehouse.entity.Appliance;
import edu.itpu.household_appliances_warehouse.services.ApplianceServicesSearch;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class SearchLayout implements SearchLayoutInterface{
    public <T extends Appliance> void innerSearch(List<? extends Appliance> dao) throws IOException {
        System.out.println("""
                            : By which parameter you want to search :
                            1.ID
                            2.Name
                            3.Brand
                            4.Condition
                            5.Price (min,max)
                            6.Exit to main menu
                            """);
        Scanner searchScanner = new Scanner(System.in);
        String inSearchCommand = searchScanner.nextLine();
        searchScanner = new Scanner(System.in);
        ApplianceServicesSearch<T> appliance = new ApplianceServicesSearch<>((List<T>) dao);
        switch (inSearchCommand) {
            case "1":{
                System.out.print("Enter ID: ");
                int idSearch = searchScanner.nextInt();
                System.out.println(appliance.getSearchById(idSearch));
            }break;
            case "2":{
                System.out.print("Enter Name: ");
                String nameSearch = searchScanner.nextLine();
                System.out.println(appliance.getSearchByName(nameSearch));
            }break;
            case "3":{
                System.out.print("Enter Brand: ");
                String brandSearch = searchScanner.nextLine();
                System.out.println(appliance.getSearchByBrand(brandSearch));
            }break;
            case "5":{
                System.out.println("In the list max price is = " + dao.stream().max(Comparator.comparing(Appliance::getPrice)).get().getPrice() + "\n"
                        + "In the list min price is = " + dao.stream().min(Comparator.comparing(Appliance::getPrice)).get().getPrice() + "\n"
                );
                System.out.print("Enter Price (min): ");
                long minPrice = searchScanner.nextLong();
                System.out.print("Enter Price (max): ");
                long maxPrice = searchScanner.nextLong();
                System.out.println(appliance.getSearchByPrice(minPrice,maxPrice));
            }break;
            case "4":{
                System.out.print("Enter Condition type (Used | New): ");
                String conSearch = searchScanner.nextLine();
                System.out.println(appliance.getSearchByCondition(conSearch));
            }break;
            case "6":{
                System.out.println("exit to main menu");
            }break;
        }
    }
}
