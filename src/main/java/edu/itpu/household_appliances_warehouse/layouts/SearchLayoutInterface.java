package edu.itpu.household_appliances_warehouse.layouts;

import edu.itpu.household_appliances_warehouse.entity.Appliance;

import java.io.IOException;
import java.util.List;

public interface SearchLayoutInterface {
    <T extends Appliance> void innerSearch(List<? extends Appliance> dao) throws IOException;
}
