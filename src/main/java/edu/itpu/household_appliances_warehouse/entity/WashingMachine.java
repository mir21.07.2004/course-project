package edu.itpu.household_appliances_warehouse.entity;

import java.util.Objects;

public class WashingMachine extends Appliance{
    private int waterConsumption;
    private int capacity;
    public WashingMachine(String productType,long price,String name, String brand,int productId,double weight,double height,double width, int energyConsumption,int capacity, int waterConsumption){
        super(productType,price,name,brand,productId,weight,height,width,energyConsumption);
        this.setCapacity(capacity);
        this.setWaterConsumption(waterConsumption);
    }

    public void setWaterConsumption(int waterConsumption) {
        this.waterConsumption = waterConsumption;
    }

    public int getWaterConsumption() {
        return waterConsumption;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WashingMachine appliance = (WashingMachine) o;
        return super.equals(appliance) && capacity == appliance.getCapacity() && waterConsumption == appliance.getWaterConsumption();
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity,waterConsumption,super.hashCode());
    }

    @Override
    public String toString() {
        return "WashingMachine = { " +
                super.toString() +
                "capacity=" + getCapacity() + ", " +
                "waterConsumption=" + getWaterConsumption() + ", " + " }" + "\n";
    }
}
