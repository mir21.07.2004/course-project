package edu.itpu.household_appliances_warehouse.entity;

import java.util.Objects;

public class Television extends Appliance{
    private String displayTechnology;
    private int refreshRate;
    private boolean smartTV;
    public Television(String productType, long price, String name, String brand, int productId, double weight, double height, double width, int energyConsumption) {
        super(productType,price,name,brand,productId,weight,height,width,energyConsumption);
    }

    public String getDisplayTechnology() {
        return displayTechnology;
    }

    public void setDisplayTechnology(String displayTechnology) {
        this.displayTechnology = displayTechnology;
    }

    public int getRefreshRate() {
        return refreshRate;
    }

    public void setRefreshRate(int refreshRate) {
        this.refreshRate = refreshRate;
    }

    public void setSmartTV(boolean smartTV) {
        this.smartTV = smartTV;
    }
    public boolean getSmartTV() {
        return smartTV ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Television appliance = (Television) o;
        return  displayTechnology.equals(appliance.getDisplayTechnology())
                && refreshRate == appliance.getRefreshRate()
                && smartTV == appliance.getSmartTV()
                && super.equals(appliance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(displayTechnology, refreshRate, smartTV, super.hashCode());
    }

    @Override
    public String toString() {
        return "Television = { " +
                 super.toString() +
                "displayTechnology=" + getDisplayTechnology() + ", " +
                "refreshRate=" + getRefreshRate() + ", " +
                "smartTV" + getSmartTV() + ", " + " }" + "\n";
    }
}
