package edu.itpu.household_appliances_warehouse.entity;

import java.util.Objects;

public class Refrigerator extends Appliance{
    private int capacity;
    private int noise;
    public Refrigerator(
            String productType, long price, String name, String brand,
            int productId, double weight, double height, double width,
            int energyConsumption, int capacity, int noise
    ){
        super(productType,price,name,brand,productId,weight,height,width,energyConsumption);
        this.setCapacity(capacity);
        this.setNoise(noise);
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setNoise(int noise) {
        this.noise = noise;
    }

    public int getNoise() {
        return noise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Refrigerator appliance = (Refrigerator) o;
        return super.equals(appliance) && capacity == appliance.capacity && noise == appliance.getNoise();
    }

    @Override
    public int hashCode() {
        return Objects.hash(noise,capacity,super.hashCode());
    }

    @Override
    public String toString() {
        return "Refrigerator = {" +
                super.toString() +
                "capacity=" + getCapacity() + ", " +
                "noise=" + getNoise() + ", " + " }" + "\n";
    }
}
