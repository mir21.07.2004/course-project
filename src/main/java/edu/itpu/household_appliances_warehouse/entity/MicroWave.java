package edu.itpu.household_appliances_warehouse.entity;

import java.util.Objects;

public class MicroWave extends Appliance{
    private int capacity;

    public MicroWave(String productType,long price,String name, String brand,int productId,double weight,double height,double width, int energyConsumption,int capacity){
        super(productType,price,name,brand,productId,weight,height,width,energyConsumption);
        this.setCapacity(capacity);
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Refrigerator appliance = (Refrigerator) o;
        return super.equals(appliance) && capacity == appliance.getCapacity();
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity,super.hashCode());
    }

    @Override
    public String toString() {
        return "MicroWave = {" +
                super.toString() +
                "capacity=" + getCapacity() + ", " + " }" + "\n";
    }
}
