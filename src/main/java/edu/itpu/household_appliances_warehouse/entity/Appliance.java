package edu.itpu.household_appliances_warehouse.entity;
import java.util.Objects;

public class Appliance {
    private String productType;
    private long price;
    private String name;
    private String brand;
    private int productId;
    private double weight;
    private double height;
    private double width;
    private int energyConsumption;

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductType() {
        return productType;
    }
    public void setPrice(long price) {
        this.price = price;
    }

    public long getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductId() {
        return productId;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setEnergyConsumption(int energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public int getEnergyConsumption() {
        return energyConsumption;
    }

    public Appliance(String productType, long price, String name, String brand, int productId, double weight, double height, double width, int energyConsumption){
        this.setProductType(productType);
        this.setPrice(price);
        this.setName(name);
        this.setBrand(brand);
        this.setProductId(productId);
        this.setWeight(weight);
        this.setHeight(height);
        this.setWidth(width);
        this.setEnergyConsumption(energyConsumption);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Appliance appliance = (Appliance) o;
        return productType.equals(appliance.getProductType())
                && price == appliance.getPrice()
                && productId == appliance.getProductId()
                && Double.compare(appliance.getWeight(), weight) == 0
                && Double.compare(appliance.getHeight(), height) == 0
                && Double.compare(appliance.getWidth(), width) == 0
                && energyConsumption == appliance.getEnergyConsumption()
                && brand.equalsIgnoreCase(appliance.getBrand())
                && name.equalsIgnoreCase(appliance.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(productType, price, name,brand,productId ,weight, height, width, energyConsumption);
    }

    @Override
    public String toString() {
        return "productType=" + productType + ", " +
                "Id" + productId + ", " +
                "brand=" + brand + ", " +
                "name=" + name + ", " +
                "price=" + price + ", " +
                "weight=" + weight + ", " +
                "height=" + height + ", " +
                "width=" + width + ", " +
                "energyConsumption=" + energyConsumption + ", ";
    }
}
