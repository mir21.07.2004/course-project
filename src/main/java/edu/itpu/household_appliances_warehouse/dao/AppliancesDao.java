package edu.itpu.household_appliances_warehouse.dao;

import edu.itpu.household_appliances_warehouse.entity.Appliance;
import java.io.IOException;
import java.util.List;

public interface AppliancesDao {
    List<String[]> convertFileToStringArr() throws IOException;
    List<? extends Appliance> getAppliancesList() throws IOException;
    List<String> getAppliancesHeaders() throws IOException;
}
