package edu.itpu.household_appliances_warehouse.dao;

import edu.itpu.household_appliances_warehouse.entity.Appliance;
import edu.itpu.household_appliances_warehouse.entity.DishWasher;
import edu.itpu.household_appliances_warehouse.entity.Refrigerator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RefrigeratorDao implements AppliancesDao{
    @Override
    public List<String[]> convertFileToStringArr() throws IOException {
        List<String[]> dataLine = new ArrayList<>();
        String fileName = "src/main/java/edu/itpu/household_appliances_warehouse/resources/Refrigerator.csv";
        try (BufferedReader dataReader = new BufferedReader(new FileReader(fileName))){
            String line = "";
            while ((line = dataReader.readLine()) != null) {
                String[] product = line.split(";");
                dataLine.add(product);
//                System.out.println(line);
            }
            dataReader.close();
        };
        return dataLine;
    }

    @Override
    public List<Refrigerator> getAppliancesList() throws IOException {
        List<String[]> stringArrays = convertFileToStringArr();
        List<Refrigerator> RefrigeratorList = new ArrayList<>();

        for(int i = 1; i < stringArrays.size(); i++) {
            String[] currentStringArr = stringArrays.get(i);
            Refrigerator refrigerator = new Refrigerator(
                    currentStringArr[0],
                    Long.parseLong(currentStringArr[1]),
                    currentStringArr[2],
                    currentStringArr[3],
                    Integer.parseInt(currentStringArr[4]),
                    Double.parseDouble(currentStringArr[5]),
                    Double.parseDouble(currentStringArr[6]),
                    Double.parseDouble(currentStringArr[7]),
                    Integer.parseInt(currentStringArr[8]),
                    Integer.parseInt(currentStringArr[9]),
                    Integer.parseInt(currentStringArr[10])
            );
            RefrigeratorList.add(refrigerator);
        }
        return RefrigeratorList;
    }

    @Override
    public List<String> getAppliancesHeaders() throws IOException {
        List<String[]> stringArrays = convertFileToStringArr();
        return Arrays.stream(stringArrays.get(0)).toList();
    }
}
